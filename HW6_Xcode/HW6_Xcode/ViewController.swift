//
//  ViewController.swift
//  HW6_Xcode
//
//  Created by Gilbert Hopkins on 5/13/19.
//  Copyright © 2019 Hop. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var displayLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func preeMeAction(_ sender: UIButton) {
        displayLabel.text = "Hello World";
        self.changeLabelTimeOut(text: "Thank you for pressing me.")
    }
    
    func changeLabelTimeOut(text: String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 3){
            self.displayLabel.text = text;
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.displayLabel.text = "";
            }
        }
    }
}

